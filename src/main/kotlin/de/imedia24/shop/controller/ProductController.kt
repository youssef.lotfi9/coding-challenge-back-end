package de.imedia24.shop.controller

import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/")
class ProductController(private val productService: ProductService) {

    @ApiOperation("Get a product by sku")
    @ApiResponses(
        ApiResponse(code = 200, message = "Successfully retrieved product"),
        ApiResponse(code = 404, message = "Product not found")
    )
    @GetMapping("/products/{sku}", produces = ["application/json;charset=utf-8"])
    fun getProductsBySku(
        @PathVariable("sku") sku: String
    ): ResponseEntity<ProductResponse> {
        val product = productService.findProductBySku(sku)
        return if (product == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(product)
        }
    }

    @ApiOperation("Get a product by skus")
    @GetMapping("/products")
    fun getProductDetailsBySkus(@RequestParam skus: String): ResponseEntity<List<ProductResponse>> {
        val productDetailsBySkus = productService.getProductDetailsBySkus(skus)
        return ResponseEntity.ok(productDetailsBySkus)

    }

    @ApiOperation("Get all products")
    @GetMapping("/products/all", produces = ["application/json;charset=utf-8"])
    fun getAllProducts(
    ): ResponseEntity<List<ProductResponse>> {
        val products = productService.findAllProducts()
        return if (products == null) {
            ResponseEntity.notFound().build()
        } else {
            ResponseEntity.ok(products)
        }
    }

    @ApiOperation("Add a new product")
    @PostMapping("/products/")
    fun addProduct(
        @RequestBody productResponse: ProductResponse
    ) {
        productService.addProduct(productResponse);
    }

    @ApiOperation("update product")
    @PutMapping("/products/{sku}")
    fun updateProduct(
        @PathVariable("sku") sku: String,
        @RequestBody productResponse: ProductResponse
    ) {
        productService.updateProduct(sku, productResponse)
    }
}
