package de.imedia24.shop.service

import de.imedia24.shop.controller.ProductController
import de.imedia24.shop.db.entity.ProductEntity
import de.imedia24.shop.db.repository.ProductRepository
import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.domain.product.ProductResponse.Companion.toProductResponse
import de.imedia24.shop.mapper.ProductMapper
import org.slf4j.LoggerFactory
import org.springframework.stereotype.Service
import java.lang.Exception
import java.time.ZonedDateTime

@Service
class ProductService(private val productRepository: ProductRepository, private val productMapper: ProductMapper) {

    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    fun findProductBySku(sku: String): ProductResponse? {
        logger.info("finding product by SKU: $sku")
        val product = productRepository.findBySku(sku);
        return product?.let {
            product.description?.let { it1 ->
                ProductResponse(
                    product.sku, it.name,
                    it1, product.price, product.quantity
                )
            }
        };
    }

    fun addProduct(productResponse: ProductResponse) {
        logger.info("adding product : $productResponse")
        val product = productRepository.findBySku(productResponse.sku)
        if (product != null) {
            throw Exception("Product already exists with id ${productResponse.sku}")
        }
        val productEntity = ProductEntity(
            productResponse.sku,
            productResponse.name,
            productResponse.description,
            productResponse.price,
            productResponse.quantity,
            ZonedDateTime.now(),
            ZonedDateTime.now()
        )
        productRepository.save(productEntity);
    }

    fun updateProduct(sku: String, productResponse: ProductResponse): ProductResponse {
        logger.info("updating product with sku: $sku")
        val product = productRepository.findBySku(sku) ?: throw Exception("Product not Found")
        val productEntity = ProductEntity(
            sku,
            productResponse.name,
            productResponse.description,
            productResponse.price,
            productResponse.quantity,
            product.createdAt,
            ZonedDateTime.now()
        )
        return productRepository.save(productEntity).toProductResponse()
    }

    fun findAllProducts(): List<ProductResponse> {
        logger.info("getting all products")
        val findAll = productRepository.findAll()
        val x = productMapper.productsToResp(findAll);
        return x!!;
    }

    fun getProductDetailsBySkus(skus: String): List<ProductResponse> {
        val skusArray = skus.split(",")
        logger.info("getting product by skus: $skusArray")

        val productDetailsList = mutableListOf<ProductResponse>()

        for (sku in skusArray) {
            val productDetails =
                productRepository.findBySku(sku) ?: throw Exception("Product not found with SKU: $sku");
            productDetailsList.add(productDetails.toProductResponse())
        }

        return productDetailsList
    }
}
