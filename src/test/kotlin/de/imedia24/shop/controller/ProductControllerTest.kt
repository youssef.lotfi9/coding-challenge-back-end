package de.imedia24.shop.controller

import de.imedia24.shop.domain.product.ProductResponse
import de.imedia24.shop.service.ProductService
import net.bytebuddy.matcher.ElementMatchers.`is`
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.*
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.math.BigDecimal


@ExtendWith(MockitoExtension::class)
@WebMvcTest(ProductController::class)
class ProductControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @MockBean
    private lateinit var service: ProductService

    @Test
    fun `should update the resource`() {
        val sku = "1"
        val updatedProduct = ProductResponse(sku, "lotfi", "updated description", BigDecimal(11), 1)
        //val product = ProductResponse(sku, "old name", "old descr", BigDecimal(10), 1)

        `when`(service.updateProduct(sku, updatedProduct)).thenReturn(updatedProduct)

        mockMvc.perform(
            MockMvcRequestBuilders.put("/products/{sku}", sku)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"sku\": 1, \"name\": \"lotfi\", \"description\": \"updated description\", \"price\": 11, \"quantity\": 1}")
        )
            .andExpect(status().isOk)
        // TODO - to be fixed

//            .andExpect(jsonPath("$.sku").value(sku))
//            .andExpect(jsonPath("$.name").value("lotfi"))
//            .andExpect(jsonPath("$.description").value("updated description"))
//            .andExpect(jsonPath("$.price").value(11))

        verify(service, times(1)).updateProduct(sku, updatedProduct)
        verifyNoMoreInteractions(service)
    }
}